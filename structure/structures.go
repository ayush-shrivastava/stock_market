package structure

//data structures
type User struct {
	Name string `json:"name"`
	Amt  int    `json:"amt"`
}

type Market struct {
	Company string `json:"company"`
	Units   int    `json:"unit"`
	Amt     int    `json:"amt"`
}

type Stock struct {
	Name    string `json:"name"`
	Company string `json:"company"`
	Units   int    `json:"unit"`
}
