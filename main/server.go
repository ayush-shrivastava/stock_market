package main

import (
	"fmt"
	"net/http"

	"github.com/ayush-shrivastava22/stock_market/controller"
	"github.com/ayush-shrivastava22/stock_market/database"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

//controller function

func main() {

	database.CreateDB()
	myRouter := mux.NewRouter()
	fmt.Println("starting server ....")
	myRouter.HandleFunc("/remAmt", controller.RemAmt).Methods("GET")
	myRouter.HandleFunc("/createUser", controller.CreateUser).Methods("POST")
	myRouter.HandleFunc("/myStocks", controller.MyStocks).Methods("GET")
	myRouter.HandleFunc("/market", controller.Market).Methods("GET")

	myRouter.HandleFunc("/buy", controller.Buy).Methods("POST")
	myRouter.HandleFunc("/sell", controller.Sell).Methods("POST")
	http.ListenAndServe(":3000", myRouter)

}
