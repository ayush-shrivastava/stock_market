package main

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/ayush-shrivastava22/stock_market/controller"
	"github.com/ayush-shrivastava22/stock_market/database"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func Router() *mux.Router {
	myRouter := mux.NewRouter()
	database.CreateDB()
	myRouter.HandleFunc("/remAmt", controller.RemAmt).Methods("GET")
	myRouter.HandleFunc("/createUser", controller.CreateUser).Methods("POST")
	myRouter.HandleFunc("/myStocks", controller.MyStocks).Methods("GET")
	myRouter.HandleFunc("/market", controller.Market).Methods("GET")

	myRouter.HandleFunc("/buy", controller.Buy).Methods("POST")
	myRouter.HandleFunc("/sell", controller.Sell).Methods("POST")
	return myRouter
}

func getTest(t *testing.T, route string, status int) {
	request, _ := http.NewRequest("GET", route, nil)
	response := httptest.NewRecorder()
	Router().ServeHTTP(response, request)
	fmt.Println(response)
	fmt.Println(response.Code)
	assert.Equal(t, status, response.Code, "OK response expected")
}

func TestRemAmt(t *testing.T) {
	var url string = "/remAmt?name=abhi"
	getTest(t, url, 200)
}

func TestMyStocks(t *testing.T) {
	var url string = "/myStocks?name=ayush"
	getTest(t, url, 200)
}

func TestMarket(t *testing.T) {
	var url string = "/market"
	getTest(t, url, 200)
}

func TestCreateUser(t *testing.T) {
	var url string = "/createUser"
	data := []byte(`{"name":"testBot"}`)

	request, _ := http.NewRequest("POST", url, bytes.NewBuffer(data))
	response := httptest.NewRecorder()
	Router().ServeHTTP(response, request)
	assert.Equal(t, 400, response.Code, "OK response expected")
}

func TestBuyAndSell(t *testing.T) {

	//Testing buy api
	var url string = "/buy"
	data := []byte(`{"name":"testBot","company":"Uber","unit":1}`)
	request, _ := http.NewRequest("POST", url, bytes.NewBuffer(data))
	response := httptest.NewRecorder()
	Router().ServeHTTP(response, request)
	assert.Equal(t, 200, response.Code, "OK response expected")

	//Testing sell api
	url = "/sell"
	data = []byte(`{"name":"testBot","company":"Uber","unit":1}`)
	request, _ = http.NewRequest("POST", url, bytes.NewBuffer(data))
	response = httptest.NewRecorder()
	Router().ServeHTTP(response, request)
	assert.Equal(t, 200, response.Code, "OK response expected")
}
