package controller

import (
	"fmt"
	"strconv"

	"github.com/ayush-shrivastava22/stock_market/database"
)

func buyStock(name string, company string, unit int) (string, int) {
	remFund, status := database.FindUsers(name)
	if status == 400 {
		return "User does not exit", 400
	}

	fund, _ := strconv.Atoi(remFund)

	marketStock, status1 := database.MarketInfo(company)

	if status1 != 200 {
		return "", status1
	}

	if marketStock.Units < unit {
		return "Sufficient Units Not Available", 400
	}

	buyingPrice := unit * marketStock.Amt
	if buyingPrice > fund {
		return "Not Sufficient Funds", 400
	}

	status2 := database.UpdateUser(name, fund-buyingPrice)
	status3 := database.UpdateMarket(company, marketStock.Units-unit)
	status4 := database.AddStock(name, company, unit)
	fmt.Println(status2, status3, status4)
	if status2 != 200 || status3 != 200 || status4 != 200 {
		return "Unsuccessful", 500
	}
	return "Transaction Successful", 200

}

func sellStock(name string, cmp string, units int) (string, int) {
	remFund, status := database.FindUsers(name)
	if status == 400 {
		return "User does not exit", 400
	}
	fund, _ := strconv.Atoi(remFund)
	remUnits, status := database.FindStock(name, cmp)
	if status != 200 {
		return "", 500
	}
	fmt.Println(remUnits)
	if remUnits < units {
		return "Not Sufficient units to sell", 400
	}
	marketStock, status1 := database.MarketInfo(cmp)
	if status1 != 200 {
		return "", status
	}
	status2 := database.UpdateMarket(cmp, marketStock.Units+units)
	status3 := database.UpdateUser(name, fund+(marketStock.Amt*units))
	status4 := database.DeleteStock(name, cmp)
	var status5 int = 200
	if remUnits > units {
		status5 = database.AddStock(name, cmp, remUnits-units)
	}
	if status2 != 200 || status3 != 200 || status4 != 200 || status5 != 200 {
		return "Unsuccessful", 500
	}
	return "Successful Transactions", 200

}
