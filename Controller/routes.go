package controller

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/ayush-shrivastava22/stock_market/structure"

	"github.com/ayush-shrivastava22/stock_market/database"
)

//will create new users with initial amount 1000
func CreateUser(w http.ResponseWriter, r *http.Request) {

	var buffer, _ = ioutil.ReadAll(r.Body)
	var s structure.User
	json.Unmarshal(buffer, &s)
	fmt.Println(r.Body)
	fmt.Println("this is the value", s)
	msg, status := database.NewUser(s.Name)
	w.WriteHeader(status)
	fmt.Fprintf(w, msg)

}

//will get user remaining amout
func RemAmt(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query().Get("name")
	amt, status := database.FindUsers(name)
	w.WriteHeader(status)
	fmt.Fprintln(w, amt)

}

//end poinnt for returningg stocks for user
func MyStocks(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query().Get("name")
	msg, status := database.GetStocks(name)
	w.WriteHeader(status)
	fmt.Fprintln(w, msg)

}

//get comapany info
func Market(w http.ResponseWriter, r *http.Request) {

	data, status := database.GetMarket()
	w.WriteHeader(status)
	fmt.Fprintln(w, data)
	fmt.Println("market")

}

// buy
func Buy(w http.ResponseWriter, r *http.Request) {
	var buffer, _ = ioutil.ReadAll(r.Body)
	var s structure.Stock
	json.Unmarshal(buffer, &s)
	res, status := buyStock(s.Name, s.Company, s.Units)
	w.WriteHeader(status)
	fmt.Fprintf(w, res)

}

//sell
func Sell(w http.ResponseWriter, r *http.Request) {

	var buffer, _ = ioutil.ReadAll(r.Body)
	fmt.Println(r.Body)
	var s structure.Stock
	json.Unmarshal(buffer, &s)
	res, status := sellStock(s.Name, s.Company, s.Units)
	w.WriteHeader(status)
	fmt.Fprintf(w, res)

}
