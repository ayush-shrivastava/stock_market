package database

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"

	"github.com/ayush-shrivastava22/stock_market/structure"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

func CreateDB() {
	db, _ = sql.Open("mysql", user+":"+password+"@tcp("+ip+":"+port+")/"+schema)
}

var userInfo string = "userInfo"

func NewUser(name string) (string, int) {
	if name != "" {
		query := "INSERT INTO " + userInfo + " (name,amt) VALUES (\"" + name + "\",1000)"
		_, err := db.Query(query)
		if err != nil {
			fmt.Println(err.Error())
			if strings.Contains(err.Error(), "Duplicate") {
				return "", 400
			}
			return "", 500
		}
		return "User created", 200
	}
	return "Invalid name", 400
}

func FindUsers(name string) (string, int) {
	data, _ := db.Query("SELECT * FROM " + userInfo + " AS u WHERE u.name=\"" + name + "\"")

	var u structure.User
	for data.Next() {
		err := data.Scan(&u.Name, &u.Amt)
		if err != nil {
			return "", 500
		}
		fmt.Println(u.Name, u.Amt)
		return strconv.Itoa(u.Amt), 200
	}

	return "User Does not exits", 400
}

func UpdateUser(user string, remAmt int) int {
	amt := strconv.Itoa(remAmt)
	query := "UPDATE " + userInfo + " SET amt=" + amt + " WHERE name = \"" + user + "\""
	_, err := db.Query(query)
	if err != nil {
		return 500
	}
	return 200
}
