package database

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/ayush-shrivastava22/stock_market/structure"
)

var market string = "market"

func GetMarket() (string, int) {
	data, _ := db.Query("SELECT * FROM " + market)
	market := []structure.Market{}
	for data.Next() {

		var m structure.Market
		err := data.Scan(&m.Company, &m.Units, &m.Amt)
		if err != nil {
			return "", 500
		}
		market = append(market, m)
	}
	fmt.Println(market)
	buffer, _ := json.Marshal(market)
	x := string(buffer)
	return x, 200
}

//will returnn stocks and rem units of current comapny
func MarketInfo(c string) (structure.Market, int) {
	data, _ := db.Query("Select * FROM " + market + " AS m WHERE m.company = \"" + c + "\"")

	for data.Next() {
		var m structure.Market
		err := data.Scan(&m.Company, &m.Units, &m.Amt)
		if err != nil {
			return m, 500
		}

		return m, 200

	}

	return structure.Market{Company: "", Units: -1, Amt: 0}, 400
}

func UpdateMarket(comp string, unit int) int {
	u := strconv.Itoa(unit)
	_, err := db.Query("UPDATE " + market + " SET unit=\"" + u + "\" WHERE company=\"" + comp + "\"")
	if err != nil {
		return 500
	}
	return 200
}
