package database

import (
	"encoding/json"
	"strconv"

	"github.com/ayush-shrivastava22/stock_market/structure"
)

var holding string = "holding"

func GetStocks(name string) (string, int) {
	stocks := []structure.Stock{}
	query := "SELECT * FROM " + holding + " WHERE name = \"" + name + "\""
	data, _ := db.Query(query)

	for data.Next() {
		var s structure.Stock
		err := data.Scan(&s.Name, &s.Company, &s.Units)

		if err != nil {
			return "", 500
		}

		stocks = append(stocks, s)

	}
	buffer, _ := json.Marshal(stocks)

	return string(buffer), 200
}

func AddStock(user string, company string, unit int) int {
	u := strconv.Itoa(unit)
	query := "INSERT INTO " + holding + " (name,company,unit) VALUES (\"" + user + "\",\"" + company + "\",\"" + u + "\")"

	_, err := db.Query(query)
	if err != nil {
		q := "UPDATE " + holding + " SET unit = unit + " + u + " WHERE company = \"" + company + "\" && name = \"" + user + "\""
		_, e := db.Query(q)
		if e != nil {
			return 500
		}
		return 200
	}
	return 200
}

func DeleteStock(user string, company string) int {
	query := "DELETE FROM " + holding + " WHERE name = \"" + user + "\" && company = \"" + company + "\""
	_, err := db.Query(query)
	if err != nil {
		return 500
	}
	return 200
}

func FindStock(user string, cmp string) (int, int) {
	query := "SELECT * FROM " + holding + " WHERE name = \"" + user + "\" && company = \"" + cmp + "\""
	data, err := db.Query(query)
	if err != nil {
		return -1, 500
	}
	for data.Next() {
		var s structure.Stock
		data.Scan(&s.Company, &s.Name, &s.Units)
		return s.Units, 200
	}
	return -1, 500
}
